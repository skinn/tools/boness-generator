# <%= name %>

A Skinn Web project.

## Installation
Clone the project to a folder on your computer.
Edit your `Homestead.yaml` so that it points to this folder:
```yaml
- map: project.skinn.local
  to: /home/vagrant/Code/project.skinn.local/public
```

Don't forget to set up your database too:
```yaml
databases:
  - project
```

Install Composer dependencies:
```console
composer install
```

Install NPM dependencies with Yarn or npm:
```console
# Yarn
yarn

# NPM
npm install
```

Build the CMS and the project
```console
npm run build:all
```

## Building
Make sure that your Vagrant URL is set correctly in `config.dev.json`.

```json
{
  "name": "project",
  "url": "project.skinn.local",
  "debug": false
}

```

> ⚠️ Only set `debug` to true if you need to debug the CMS directly from the vendor folder

Build using npm scripts. The following scripts are available:
* `npm run build` - Build the CMS files.
* `npm run build:all` - Build the CMS and the project.
* `npm run watch` - Watch for changes in the project.
* `npm run production` - Build the CMS and the project for production.

## Author

Made by [skinn branding agency](https://www.skinn.be)
Developer: <%= authorName %> - [<%= authorEmail %>](mailto:<%= authorEmail %>)
